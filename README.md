# Weather

## Tech Stack
- Dagger 2 - Used to provide dependency injection
- Retrofit 2 - OkHttp3 - request/response API
- RxJava 2 - reactive programming paradigm
- LiveData - use LiveData to see UI update with data changes.
- Data Binding - bind UI components in layouts to data sources



## Weather details data

The endpoint to get weather details based on a city id:

`https://api.openweathermap.org/data/2.5/weather?id={cityId}&units={unitName}`

Example using Barcelona's city ID and `metric` units:

`https://api.openweathermap.org/data/2.5/weather?id=3128760&units=metric`
