package com.yusufcakmak.weather.presentation.detail.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.yusufcakmak.weather.R


internal class WeatherTabPagerAdapter(val context: Context) : PagerAdapter() {

    val titles = context.resources.getStringArray(R.array.tabs)

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        var resId = 0
        when (position) {
            0 -> resId = R.id.layoutWind
            1 -> resId = R.id.layoutPressure
            2 -> resId = R.id.layoutHumidity
        }
        return collection.findViewById(resId)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return titles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    override fun destroyItem(
            container: ViewGroup,
            position: Int,
            `object`: Any
    ) { // No super
    }
}