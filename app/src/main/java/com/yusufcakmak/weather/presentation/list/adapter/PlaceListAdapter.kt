package com.yusufcakmak.weather.presentation.list.adapter

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yusufcakmak.weather.R
import com.yusufcakmak.weather.ext.inflate
import com.yusufcakmak.weather.common.colorResourceToStateList
import com.yusufcakmak.weather.databinding.PlacesListItemBinding
import com.yusufcakmak.weather.domain.model.Place
import com.yusufcakmak.weather.presentation.list.viewstate.PlaceListItemViewState
import javax.inject.Inject


typealias OnPlaceClickListener = (String) -> Unit

class PlaceListAdapter @Inject constructor(private val places: List<Place>) :
    RecyclerView.Adapter<PlaceListAdapter.ViewHolder>() {

    var onItemClickListener: OnPlaceClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: PlacesListItemBinding =
            parent.inflate(R.layout.places_list_item, false)
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val place = places[position]
        holder.bind(place)

    }

    override fun getItemCount(): Int {
        return places.size
    }

    inner class ViewHolder(
        private val binding: PlacesListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            with(binding) {
                layoutWeather.setOnClickListener {
                    Log.v("TEST4","fuck")
                    onItemClickListener?.invoke(places[adapterPosition].woeId)
                }
            }
        }

        fun bind(place: Place) {
            binding.viewState = PlaceListItemViewState(place)
            val colorList = binding.viewState?.temperatureColorResource()
                ?.colorResourceToStateList(binding.root.context)
            binding.textTemperature.setTextColor(colorList)
            binding.executePendingBindings()
        }
    }
}
