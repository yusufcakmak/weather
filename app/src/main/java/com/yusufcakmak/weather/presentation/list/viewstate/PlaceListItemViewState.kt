package com.yusufcakmak.weather.presentation.list.viewstate

import com.yusufcakmak.weather.common.toColorResource
import com.yusufcakmak.weather.domain.model.Place

data class PlaceListItemViewState(val place: Place) {

    fun getCity() = place.city
    fun getCountry() = place.country
    fun getTemperature() = "${place.temperature.value}${place.temperature.unit.postFix}"
    fun temperatureColorResource(): Int = place.temperature.toColorResource()
}