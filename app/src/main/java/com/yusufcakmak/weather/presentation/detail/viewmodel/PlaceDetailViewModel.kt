package com.yusufcakmak.weather.presentation.detail.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yusufcakmak.weather.common.*
import com.yusufcakmak.weather.domain.model.Place
import com.yusufcakmak.weather.domain.usecase.FetchPlaceDetailUseCase
import com.yusufcakmak.weather.presentation.common.StatusViewState
import com.yusufcakmak.weather.presentation.detail.viewstate.PlaceDetailContentViewState
import io.reactivex.android.schedulers.AndroidSchedulers

class PlaceDetailViewModel @ViewModelInject constructor(private val fetchPlaceDetailUseCase: FetchPlaceDetailUseCase) :
    RxAwareViewModel() {

    private val placeDetail = MutableLiveData<PlaceDetailContentViewState>()
    val place: LiveData<PlaceDetailContentViewState> = placeDetail

    private val status = MutableLiveData<StatusViewState>()
    val status_: LiveData<StatusViewState> = status
    var cityId: String? = null

    fun fetchPlaces() {
        cityId?.let {
            fetchPlaceDetailUseCase
                .fetchPlaceDetail(it)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    onPlaceDetailContentResultReady(it)
                }
                .subscribe({ resource ->
                    onPlaceDetailStatusResultReady(resource)
                }, {
                })
                .also { disposable += it }
        }
    }

    private fun onPlaceDetailStatusResultReady(resource: Resource<Place?>) {
        val viewState = when (resource) {
            is Resource.Success -> StatusViewState(Status.Content)
            is Resource.Error -> StatusViewState(Status.Error(resource.exception))
            Resource.Loading -> StatusViewState(Status.Loading)
        }
        status.value = viewState
    }

    private fun onPlaceDetailContentResultReady(place: Place?) {
        placeDetail.value = PlaceDetailContentViewState(place)
    }

}