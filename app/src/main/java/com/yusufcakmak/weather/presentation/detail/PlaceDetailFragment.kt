package com.yusufcakmak.weather.presentation.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.yusufcakmak.weather.presentation.common.BaseFragment
import com.yusufcakmak.weather.databinding.FragmentDetailBinding
import com.yusufcakmak.weather.R
import com.yusufcakmak.weather.common.observeNonNull
import com.yusufcakmak.weather.presentation.common.StatusViewState
import com.yusufcakmak.weather.presentation.detail.adapter.WeatherTabPagerAdapter
import com.yusufcakmak.weather.presentation.detail.viewmodel.PlaceDetailViewModel
import com.yusufcakmak.weather.presentation.detail.viewstate.PlaceDetailContentViewState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlaceDetailFragment : BaseFragment<FragmentDetailBinding>() {

    override val layoutRes: Int = R.layout.fragment_detail

    private val viewModel: PlaceDetailViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.cityId = arguments?.getString(EXTRA_BUNDLE_ID)
        initTabsAdapter()

        viewModel.fetchPlaces()

        viewModel.place.observeNonNull(this) { places ->
            renderPlaceList(places)
        }

        viewModel.status_.observeNonNull(this) { status ->
            renderStatusResult(status)
        }
    }

    private fun renderStatusResult(statusViewState: StatusViewState) {
        if (statusViewState.isLoading()) {
            binding.loadingView.visibility = View.VISIBLE
        } else {
            binding.loadingView.visibility = View.GONE
        }

        if (statusViewState.shouldShowErrorMessage()) {
            binding.layoutError.root.visibility = View.VISIBLE
            binding.loadingView.visibility = View.GONE
            binding.layoutError.retryButton.setOnClickListener {
                viewModel.fetchPlaces()
            }
        } else {
            binding.layoutError.root.visibility = View.GONE
        }
    }

    private fun renderPlaceList(contentViewState: PlaceDetailContentViewState) {
        binding.layoutPlaceDetail.root.visibility = View.VISIBLE
        binding.viewState = contentViewState
        binding.executePendingBindings()
    }

    private fun initTabsAdapter() {
        val tabAdapter = WeatherTabPagerAdapter(requireContext())
        binding.layoutPlaceDetail.tabLayoutWeather.setupWithViewPager(binding.layoutPlaceDetail.pagerWeather)
        binding.layoutPlaceDetail.pagerWeather.adapter = tabAdapter
        binding.layoutPlaceDetail.pagerWeather.offscreenPageLimit = 3
    }

    companion object {
        const val EXTRA_BUNDLE_ID = "bundle_id"
    }
}