package com.yusufcakmak.weather.presentation.list.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yusufcakmak.weather.common.*
import com.yusufcakmak.weather.domain.model.Place
import com.yusufcakmak.weather.domain.usecase.FetchPlacesUseCase
import com.yusufcakmak.weather.presentation.common.StatusViewState
import com.yusufcakmak.weather.presentation.list.viewstate.PlaceListContentViewState
import io.reactivex.android.schedulers.AndroidSchedulers

class PlaceListViewModel @ViewModelInject constructor(private val fetchPlacesUseCase: FetchPlacesUseCase) :
    RxAwareViewModel() {

    private val placeList = MutableLiveData<PlaceListContentViewState>()
    val places: LiveData<PlaceListContentViewState> = placeList

    private val status = MutableLiveData<StatusViewState>()
    val status_: LiveData<StatusViewState> = status


    init {
        fetchPlaces()
    }

    fun fetchPlaces() {
        fetchPlacesUseCase
            .fetchPlaces()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess {
                onPlacesContentResultReady(it)
            }
            .subscribe({ resource ->
                onPlacesStatusResultReady(resource)
            }, {
            })
            .also { disposable += it }
    }

    private fun onPlacesStatusResultReady(resource: Resource<List<Place>>) {
        val viewState = when (resource) {
            is Resource.Success -> StatusViewState(Status.Content)
            is Resource.Error -> StatusViewState(Status.Error(resource.exception))
            Resource.Loading -> StatusViewState(Status.Loading)
        }
        status.value = viewState
    }

    private fun onPlacesContentResultReady(list: List<Place>) {
        placeList.value = PlaceListContentViewState(list)
    }

}