package com.yusufcakmak.weather.presentation.list.viewstate

import com.yusufcakmak.weather.domain.model.Place

data class PlaceListContentViewState(
    val list: List<Place>
) {

}