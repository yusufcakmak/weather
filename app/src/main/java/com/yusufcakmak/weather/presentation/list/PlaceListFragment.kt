package com.yusufcakmak.weather.presentation.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.yusufcakmak.weather.R
import com.yusufcakmak.weather.presentation.common.BaseFragment
import com.yusufcakmak.weather.common.observeNonNull
import com.yusufcakmak.weather.databinding.FragmentListBinding
import com.yusufcakmak.weather.presentation.common.StatusViewState
import com.yusufcakmak.weather.presentation.detail.PlaceDetailFragment.Companion.EXTRA_BUNDLE_ID
import com.yusufcakmak.weather.presentation.list.adapter.PlaceListAdapter
import com.yusufcakmak.weather.presentation.list.viewstate.PlaceListContentViewState
import com.yusufcakmak.weather.presentation.list.viewmodel.PlaceListViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PlaceListFragment : BaseFragment<FragmentListBinding>() {

    override val layoutRes: Int = R.layout.fragment_list

    private lateinit var placesAdapter: PlaceListAdapter

    private val viewModel: PlaceListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.places.observeNonNull(this) { places ->
            renderPlaceList(places)
        }

        viewModel.status_.observeNonNull(this) { status ->
            renderStatusResult(status)
        }

    }

    private fun renderStatusResult(statusViewState: StatusViewState) {
        if (statusViewState.isLoading()) {
            binding.loadingView.visibility = View.VISIBLE
        } else {
            binding.loadingView.visibility = View.GONE
        }

        if (statusViewState.shouldShowErrorMessage()) {
            binding.layoutPlacesError.root.visibility = View.VISIBLE
            initRetry()
        } else {
            binding.layoutPlacesError.root.visibility = View.GONE
        }
        binding.viewState = statusViewState
        binding.executePendingBindings()
    }

    private fun renderPlaceList(contentViewState: PlaceListContentViewState) {
        placesAdapter = PlaceListAdapter(contentViewState.list)
        binding.placesList.placesRecyclerView.adapter = placesAdapter

        with(placesAdapter) {
            onItemClickListener = {
                findNavController().navigate(R.id.fragment_detail, Bundle().apply {
                    putString(EXTRA_BUNDLE_ID, it)
                })
            }
        }
    }

    private fun initRetry() {
        binding.layoutPlacesError.retryButton.setOnClickListener {
            viewModel.fetchPlaces()
        }
    }
}