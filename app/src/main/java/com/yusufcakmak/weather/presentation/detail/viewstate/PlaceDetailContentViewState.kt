package com.yusufcakmak.weather.presentation.detail.viewstate

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Spanned
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.yusufcakmak.weather.R
import com.yusufcakmak.weather.common.toWeatherIconResource
import com.yusufcakmak.weather.domain.model.Place
import com.yusufcakmak.weather.ext.convertToReadableDate

data class PlaceDetailContentViewState(
    val place: Place?
) {

    val windSpeed = "${place?.wind?.windSpeedValue} ${place?.wind?.speedUnit?.postfix}"
    val windDegree = "${place?.wind?.windDegreeValue} ${place?.wind?.degreeUnit?.postfix}"
    val humidity = "${place?.humidity?.unit?.prefix} ${place?.humidity?.humidityValue}"
    val temperature = "${place?.temperature?.value} ${place?.temperature?.unit?.postFix}"
    val minTemperature = "${place?.temperature?.minValue} ${place?.temperature?.unit?.postFix}"
    val maxTemperature = "${place?.temperature?.maxValue} ${place?.temperature?.unit?.postFix}"
    val pressure = "${place?.pressure?.pressureValue} ${place?.pressure?.unit?.postFix}"
    val latitudeValue = "${place?.location?.latitude}"
    val longitudeValue = "${place?.location?.longitude}"
    val createdDate = place?.recordDate?.convertToReadableDate()
    val weatherIconResource = place?.toWeatherIconResource()

    fun getCity() = place?.city

    fun getWeatherIconDrawable(context: Context): Drawable? {
        return weatherIconResource?.let { ContextCompat.getDrawable(context, it) }
    }


    fun getMinMaxFormattedValue(context: Context): String {
        return context.getString(
            R.string.min_max_weather_formatter,
            minTemperature,
            maxTemperature
        )
    }

    fun getWindSpeedText(context: Context): Spanned {
        return convertTextToSpanned(
            R.string.wind_speed,
            windSpeed,
            context
        )
    }

    fun getWindDirectionText(context: Context): Spanned {
        return convertTextToSpanned(R.string.wind_direction, windDegree, context)
    }

    fun getPressureText(context: Context): Spanned {
        return convertTextToSpanned(R.string.atmoshperic_pressure, pressure, context)
    }

    fun getHumidityText(context: Context): Spanned {
        return convertTextToSpanned(R.string.humidity_with_param, humidity, context)
    }

    fun getLatitudeText(context: Context): Spanned {
        return convertTextToSpanned(R.string.latitude_with_param, latitudeValue, context)
    }

    fun getLongitudeText(context: Context): Spanned {
        return convertTextToSpanned(R.string.longitude_with_param, longitudeValue, context)
    }

    private fun convertTextToSpanned(resId: Int, value: String, context: Context): Spanned {
        return HtmlCompat.fromHtml(
            context.getString(resId, value),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
    }
}