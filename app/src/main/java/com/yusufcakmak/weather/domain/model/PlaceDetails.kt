package com.yusufcakmak.weather.domain.model

data class PlaceDetails(
        val type: Int?,
        val id: Int?,
        val countryCode: String?,
        val sunrise: Long?,
        val sunset: Long?
)