package com.yusufcakmak.weather.domain.model


data class Place(val woeId: String,
                 val city: String,
                 val country: String,
                 val temperature: Temperature,
                 val humidity: Humidity,
                 val wind: Wind,
                 val pressure: Pressure,
                 val recordDate: Long,
                 val location: Location,
                 val weatherIconId: String,
                 val weatherCode: Int)