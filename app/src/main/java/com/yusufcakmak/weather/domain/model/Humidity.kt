package com.yusufcakmak.weather.domain.model

data class Humidity(
        val humidityValue: Int,
        val unit: HumidityUnit
)

sealed class HumidityUnit(val prefix: String) {
    object Percentange : HumidityUnit(prefix = "%")
}