package com.yusufcakmak.weather.domain.model

data class Wind(
    val windSpeedValue: Double,
    val speedUnit: WindSpeedUnit,
    val windDegreeValue: Int,
    val degreeUnit: WindDegreeUnit
)

sealed class WindSpeedUnit(val postfix: String) {
    object Speed : WindSpeedUnit(postfix = "meter/sec")
}

sealed class WindDegreeUnit(val postfix: String) {
    object Degree : WindDegreeUnit(postfix = "degree")
}