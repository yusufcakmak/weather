package com.yusufcakmak.weather.domain.model

import com.google.gson.annotations.SerializedName

data class PlaceWeather(
        val id: Int?,
        val name: String?,
        val description: String?,
        val icon: String?
)