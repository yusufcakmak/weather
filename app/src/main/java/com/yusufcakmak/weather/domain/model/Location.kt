package com.yusufcakmak.weather.domain.model

data class Location(val latitude: Double?,
                    val longitude: Double?)