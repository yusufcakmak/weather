package com.yusufcakmak.weather.domain.usecase

import com.yusufcakmak.weather.common.Resource
import com.yusufcakmak.weather.common.map
import com.yusufcakmak.weather.data.local.repository.LocalDataRepository
import com.yusufcakmak.weather.data.remote.repository.WeatherRepository
import com.yusufcakmak.weather.domain.mapper.PlaceGroupMapper
import com.yusufcakmak.weather.domain.model.Place
import io.reactivex.Observable
import javax.inject.Inject

class FetchPlaceDetailUseCase @Inject constructor(
    var remoteRepository: WeatherRepository,
    var localRepository : LocalDataRepository,
    var placeMapper: PlaceGroupMapper
) {

    fun fetchPlaceDetail(cityId : String): Observable<Resource<Place?>> {
        return remoteRepository
            .fetchPlaceDetail(cityId)
            .map { resource ->
                resource.map { response ->
                    placeMapper.convertToPlace(response)
                }
            }.startWith(Resource.Loading)
    }
}