package com.yusufcakmak.weather.domain.model

data class PlaceWind (
        val speed: Double,
        val degree : Int
)