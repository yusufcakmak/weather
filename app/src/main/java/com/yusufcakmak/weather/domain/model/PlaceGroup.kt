package com.yusufcakmak.weather.domain.model


data class PlaceGroup(
        val count: Int?,
        val places: List<Place>?
)