package com.yusufcakmak.weather.domain.model

data class PlaceParameters(
        val temperature: Float?,
        val pressure: Int?,
        val humidity: Int?,
        val minTemperature: Float?,
        val maxTemperature: Float?
)