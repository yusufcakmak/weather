package com.yusufcakmak.weather.domain.mapper

import com.yusufcakmak.weather.common.Mapper
import com.yusufcakmak.weather.data.remote.model.PlaceGroupResponse
import com.yusufcakmak.weather.data.remote.model.PlaceResponse
import com.yusufcakmak.weather.domain.model.*
import java.util.*
import javax.inject.Inject

class PlaceGroupMapper @Inject constructor() : Mapper<PlaceGroupResponse, PlaceGroup> {

    override fun mapFrom(type: PlaceGroupResponse): List<Place> {
        return type.places.orEmpty().mapNotNull { convertToPlace(it) }
    }

    fun convertToPlace(placeResponse: PlaceResponse): Place? {
        val woeId = placeResponse.id?.toString() ?: return null
        val weatherCode = placeResponse.weathers?.firstOrNull()?.id ?: return null
        val weatherIconId = placeResponse.weathers.firstOrNull()?.icon ?: return null
        val city = placeResponse.name ?: return null
        val country = getCountryNameFromCountryCode(placeResponse.details?.countryCode) ?: return null
        val temperature = placeResponse.parameters?.temperature?.toInt() ?: return null
        val minTemperatureValue = placeResponse.parameters.minTemperature ?: return null
        val maxTemperatureValue = placeResponse.parameters.maxTemperature ?: return null
        val humidity = placeResponse.parameters.humidity ?: return null
        val windSpeed = placeResponse.wind.speed ?: return null
        val windDegree = placeResponse.wind.degree ?: return null
        val pressure = placeResponse.parameters.pressure ?: return null
        val recordTime = placeResponse.recordDateTime
        val lat = placeResponse.location?.latitude ?: return null
        val lon = placeResponse.location.longitude ?: return null

        return Place(
            woeId = woeId,
            city = city,
            country = country,
            temperature = Temperature(
                value = temperature,
                minValue = minTemperatureValue,
                maxValue = maxTemperatureValue,
                unit = TemperatureUnit.Celsius),
            humidity = Humidity(
                humidityValue = humidity,
                unit = HumidityUnit.Percentange
            ),
            wind = Wind(
                windSpeedValue = windSpeed,
                speedUnit = WindSpeedUnit.Speed,
                windDegreeValue = windDegree,
                degreeUnit = WindDegreeUnit.Degree
            ),
            pressure = Pressure(
                pressureValue = pressure,
                unit = PressureUnit.SeaLevel
            ),
            recordDate = recordTime,
            location = Location(
                latitude = lat,
                longitude = lon
            ),
            weatherIconId = weatherIconId,
            weatherCode = weatherCode
        )
    }

    private fun getCountryNameFromCountryCode(countryCode: String?): String? {
        val code = countryCode ?: return null
        return Locale("", code).displayCountry
    }
}