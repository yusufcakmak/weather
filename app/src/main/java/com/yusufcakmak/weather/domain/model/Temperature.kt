package com.yusufcakmak.weather.domain.model

data class Temperature(val value: Int,
                       val minValue: Float,
                       val maxValue: Float,
                       val unit: TemperatureUnit
)

sealed class TemperatureUnit(val postFix: String) {
    object Celsius : TemperatureUnit(postFix = "°C")
}