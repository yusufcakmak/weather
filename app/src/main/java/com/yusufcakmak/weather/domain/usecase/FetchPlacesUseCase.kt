package com.yusufcakmak.weather.domain.usecase

import com.yusufcakmak.weather.common.Resource
import com.yusufcakmak.weather.common.map
import com.yusufcakmak.weather.data.local.repository.LocalDataRepository
import com.yusufcakmak.weather.data.remote.repository.WeatherRepository
import com.yusufcakmak.weather.domain.mapper.PlaceGroupMapper
import com.yusufcakmak.weather.domain.model.Place
import io.reactivex.Observable
import javax.inject.Inject

class FetchPlacesUseCase @Inject constructor(
    var remoteRepository: WeatherRepository,
    var localRepository : LocalDataRepository,
    var placeMapper: PlaceGroupMapper
) {

    fun fetchPlaces(): Observable<Resource<List<Place>>> {
        val ids = localRepository.fetchLocationsIds()
        return remoteRepository
            .fetchPlaces(ids)
            .map { resource ->
                resource.map { response ->
                    placeMapper.mapFrom(response)
                }
            }.startWith(Resource.Loading)
    }
}