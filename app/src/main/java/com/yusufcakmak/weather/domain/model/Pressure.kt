package com.yusufcakmak.weather.domain.model

data class Pressure(
        val pressureValue: Int,
        val unit: PressureUnit
)

sealed class PressureUnit(val postFix: String) {
    object SeaLevel : PressureUnit(postFix = "hPa")
}