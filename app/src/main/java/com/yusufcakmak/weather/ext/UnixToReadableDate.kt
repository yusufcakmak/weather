package com.yusufcakmak.weather.ext

import java.text.SimpleDateFormat
import java.util.*

fun Long.convertToReadableDate() : String{
    val date = Date(this * 1000L)
    val createdDateFormat = SimpleDateFormat("dd MMM HH:mm", Locale.getDefault())
    return createdDateFormat.format(date)
}