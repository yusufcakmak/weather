package com.yusufcakmak.weather.ext

import android.graphics.drawable.Drawable
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("app:imageDrawable")
fun set(view: AppCompatImageView, drawable: Drawable?) {
    view.setImageDrawable(drawable)
}