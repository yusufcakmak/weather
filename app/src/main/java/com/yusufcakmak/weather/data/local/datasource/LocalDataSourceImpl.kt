package com.yusufcakmak.weather.data.local.datasource

import javax.inject.Inject

class LocalDataSourceImpl @Inject constructor(): LocalDataSource {

    private val locationIds = listOf(
        "2643743",
        "2950159",
        "3128760",
        "2267057",
        "2964574",
        "2618425",
        "524901",
        "5128581",
        "5375480",
        "2147714",
        "292223",
        "2988507"
    )

    private val ids = locationIds.joinToString(separator = ",")

    override fun fetchLocations(): String {
        return ids
    }
}