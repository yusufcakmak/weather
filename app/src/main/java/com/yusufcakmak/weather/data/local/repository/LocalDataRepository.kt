package com.yusufcakmak.weather.data.local.repository

interface LocalDataRepository {

    fun fetchLocationsIds() : String
}