package com.yusufcakmak.weather.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceResponse(
    @SerializedName("coord") val location: LocationResponse?,
    @SerializedName("sys") val details: PlaceDetailsResponse?,
    @SerializedName("main") val parameters: PlaceParametersResponse?,
    @SerializedName("weather") val weathers: List<PlaceWeatherResponse>?,
    @SerializedName("wind") val wind: PlaceWindResponse,
    @SerializedName("clouds") val clouds: PlaceCloudsResponse,
    @SerializedName("id") val id: Int?,
    @SerializedName("dt") val recordDateTime: Long,
    @SerializedName("name") val name: String?
)