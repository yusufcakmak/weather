package com.yusufcakmak.weather.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceParametersResponse(
        @SerializedName("temp") val temperature: Float?,
        @SerializedName("pressure") val pressure: Int?,
        @SerializedName("humidity") val humidity: Int?,
        @SerializedName("temp_min") val minTemperature: Float?,
        @SerializedName("temp_max") val maxTemperature: Float?
)