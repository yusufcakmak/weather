package com.yusufcakmak.weather.data.remote.datasource

import com.yusufcakmak.weather.data.remote.WeatherAPI
import javax.inject.Inject

class WeatherRemoteDataSource @Inject constructor(private val weatherApi: WeatherAPI) {

    fun fetchPlaces(ids: String, units: String) = weatherApi.fetchPlaces(ids, units)

    fun fetchPlaceDetail(id: String, units: String) = weatherApi.fetchPlaceDetail(id, units)

}