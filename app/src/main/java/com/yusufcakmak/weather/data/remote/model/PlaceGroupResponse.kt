package com.yusufcakmak.weather.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceGroupResponse(
        @SerializedName("cnt") val count: Int?,
        @SerializedName("list") val places: List<PlaceResponse>?
)