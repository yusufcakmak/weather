package com.yusufcakmak.weather.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceCloudsResponse(
        @SerializedName("all")
        val all: Int
)