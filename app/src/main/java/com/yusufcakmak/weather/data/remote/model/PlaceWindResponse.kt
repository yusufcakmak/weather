package com.yusufcakmak.weather.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceWindResponse (
        @SerializedName("speed")
        val speed: Double?,
        @SerializedName("deg")
        val degree : Int?
)