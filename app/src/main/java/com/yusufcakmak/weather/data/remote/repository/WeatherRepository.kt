package com.yusufcakmak.weather.data.remote.repository

import com.yusufcakmak.weather.common.Resource
import com.yusufcakmak.weather.data.remote.model.PlaceResponse
import com.yusufcakmak.weather.data.remote.model.PlaceGroupResponse
import io.reactivex.Observable
import javax.inject.Inject


interface WeatherRepository{

    fun fetchPlaces(ids: String, units: String = "metric"): Observable<Resource<PlaceGroupResponse>>

    fun fetchPlaceDetail(id: String, units: String = "metric"): Observable<Resource<PlaceResponse>>
}