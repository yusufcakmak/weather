package com.yusufcakmak.weather.data.di

import com.yusufcakmak.weather.data.local.repository.LocalDataRepository
import com.yusufcakmak.weather.data.local.repository.LocalDataRepositoryImpl
import com.yusufcakmak.weather.data.local.datasource.LocalDataSource
import com.yusufcakmak.weather.data.local.datasource.LocalDataSourceImpl
import com.yusufcakmak.weather.data.remote.WeatherAPI
import com.yusufcakmak.weather.data.remote.datasource.WeatherRemoteDataSource
import com.yusufcakmak.weather.data.remote.repository.WeatherRepository
import com.yusufcakmak.weather.data.remote.repository.WeatherRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalDataModule {

    @Provides
    @Singleton
    fun provideRemoteDataSource(weatherAPI: WeatherAPI): WeatherRemoteDataSource {
        return WeatherRemoteDataSource(weatherAPI)
    }

    @Provides
    @Singleton
    fun provideRemoteRepository(remoteDataSource: WeatherRemoteDataSource): WeatherRepository {
        return WeatherRepositoryImpl(remoteDataSource)
    }

    @Provides
    @Singleton
    fun provideLocalDataSource(): LocalDataSource {
        return LocalDataSourceImpl()
    }

    @Provides
    @Singleton
    fun provideLocalRepository(localDataSource: LocalDataSource): LocalDataRepository {
        return LocalDataRepositoryImpl(localDataSource)
    }
}