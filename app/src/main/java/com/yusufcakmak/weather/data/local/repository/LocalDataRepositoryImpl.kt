package com.yusufcakmak.weather.data.local.repository

import com.yusufcakmak.weather.data.local.datasource.LocalDataSource
import javax.inject.Inject

class LocalDataRepositoryImpl @Inject constructor(private val localDataSource: LocalDataSource) :
    LocalDataRepository {

    override fun fetchLocationsIds(): String {
        return localDataSource.fetchLocations()
    }
}