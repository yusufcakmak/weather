package com.yusufcakmak.weather.data.remote.model

import com.google.gson.annotations.SerializedName

data class PlaceWeatherResponse(
        @SerializedName("id") val id: Int?,
        @SerializedName("main") val name: String?,
        @SerializedName("description") val description: String?,
        @SerializedName("icon") val icon: String?
)