package com.yusufcakmak.weather.data.remote

import com.yusufcakmak.weather.data.remote.model.PlaceResponse
import com.yusufcakmak.weather.data.remote.model.PlaceGroupResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    @GET("group")
    fun fetchPlaces(@Query("id") ids: String,
                    @Query("units") units: String = "metric"): Observable<PlaceGroupResponse>

    @GET("weather")
    fun fetchPlaceDetail(@Query("id") id: String,
                         @Query("units") units: String = "metric"): Observable<PlaceResponse>
}
