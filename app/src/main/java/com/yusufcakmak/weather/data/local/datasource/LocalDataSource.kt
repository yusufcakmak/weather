package com.yusufcakmak.weather.data.local.datasource

interface LocalDataSource {

    fun fetchLocations() : String
}