package com.yusufcakmak.weather.data.remote.repository

import com.yusufcakmak.weather.common.Resource
import com.yusufcakmak.weather.data.remote.datasource.WeatherRemoteDataSource
import com.yusufcakmak.weather.data.remote.model.PlaceResponse
import com.yusufcakmak.weather.data.remote.model.PlaceGroupResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(private val remoteDataSource: WeatherRemoteDataSource) :
    WeatherRepository {

    override fun fetchPlaces(ids: String, units: String): Observable<Resource<PlaceGroupResponse>> {
        return remoteDataSource
            .fetchPlaces(ids, units)
            .map<Resource<PlaceGroupResponse>> {
                Resource.Success(it)
            }.onErrorReturn { throwable ->
                Resource.Error(throwable)

            }.subscribeOn(Schedulers.io())
    }

    override fun fetchPlaceDetail(id: String, units: String): Observable<Resource<PlaceResponse>> {
        return remoteDataSource
            .fetchPlaceDetail(id, units)
            .map<Resource<PlaceResponse>> {
                Resource.Success(it)
            }.onErrorReturn { throwable ->
                Resource.Error(throwable)

            }.subscribeOn(Schedulers.io())
    }
}