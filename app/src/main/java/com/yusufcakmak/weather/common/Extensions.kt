package com.yusufcakmak.weather.common

import android.content.Context
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.yusufcakmak.weather.R
import com.yusufcakmak.weather.domain.model.Place
import com.yusufcakmak.weather.domain.model.Temperature
import com.yusufcakmak.weather.domain.model.TemperatureUnit
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun <T> Observable<Resource<T>>.doOnSuccess(
    onSuccess: (T) -> (Unit)
): Observable<Resource<T>> {
    return this.doOnNext {
        if (it is Resource.Success) {
            onSuccess(it.data)
        }
    }

}

fun <T> Observable<T>.remote(): Observable<Resource<T>> =
    map<Resource<T>> { Resource.Success(it) }
        .onErrorReturn { throwable ->
            Resource.Error(throwable)
        }
        .subscribeOn(Schedulers.io())
        .startWith(Resource.Loading)

fun <T> LiveData<T>.observeNonNull(owner: LifecycleOwner, observer: (t: T) -> Unit) {
    this.observe(owner, Observer {
        it?.let(observer)
    })
}

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}


fun Int.colorResourceToStateList(context: Context): ColorStateList {
    val color = ContextCompat.getColor(context, this)
    val states = array2dOfInt(1, 1)
    val colors = intArrayOf(color)
    return ColorStateList(states, colors)
}

private fun array2dOfInt(sizeOuter: Int, sizeInner: Int): Array<IntArray> = Array(sizeOuter) { IntArray(sizeInner) }


fun Place.toWeatherIconResource(): Int {
    return when (weatherIconId) {
        "01d" -> R.drawable.ic_clearsky
        "02d" -> R.drawable.ic_cloud
        "03d" -> R.drawable.ic_clouds
        "04d" -> R.drawable.ic_clouds
        "09d" -> R.drawable.ic_rain
        "10d" -> R.drawable.ic_rain
        "11d" -> R.drawable.ic_thunder
        "13d" -> R.drawable.ic_snow
        else -> R.drawable.ic_clearsky

    }
}

fun Temperature.toColorResource(): Int {
    return when (unit) {
        TemperatureUnit.Celsius -> {
            value.celsiusToColor()
        }
    }
}

private fun Int.celsiusToColor() = when (this) {
    in Int.MIN_VALUE..-5 -> R.color.temperature1
    in -4..2 -> R.color.temperature2
    in 3..14 -> R.color.temperature3
    in 15..26 -> R.color.temperature4
    else -> R.color.temperature5
}
