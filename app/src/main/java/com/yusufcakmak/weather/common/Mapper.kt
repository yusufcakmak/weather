package com.yusufcakmak.weather.common

import com.yusufcakmak.weather.domain.model.Place

interface Mapper<R, D> {
    fun mapFrom(type: R): List<Place>
}