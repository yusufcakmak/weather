object Configs {
    val applicationId = "com.yusufcakmak.weather"
    val compileSdkVersion = 30
    val minSdkVersion = 22
    val targetSdkVersion = 30
    val buildToolsVersion = "30.0.3"
    val versionCode = 1
    val versionName = "1.0.0"
    val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}