object Classpaths {
    val gradle = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
}